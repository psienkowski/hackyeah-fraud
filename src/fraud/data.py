import os
import pandas as pd

ROOT_PATH = '../'


def load_file(path):
    return pd.read_csv(path, sep=';', encoding='iso8859_2')


def load_groups():
    columns_to_drop = [
        'ID_GrupyJGP',
        'Srednia_wartosc_jednostek_grupy',
        'Srednia_wartosc_jednostek_hospitalizacji',
        'Produkty_ID_Produkty',
        'Dlugosc_hosp_dominanta',
        'Dlugosc_hosp_mediana',
        'Kod_katalogu_JGP',
        'Typ_kodu',
        'Wspolczynnik_rehospitalizacji',
        'Liczba_hospitalizacji',
    ]
    path = os.path.join(ROOT_PATH, 'data/Grupy_JGP.csv')
    df = load_file(path)
    df = df.drop(columns_to_drop, axis=1)
    df['Udzial_hospitalizacji'] = df['Udzial_hospitalizacji'].apply(lambda x: float(x.replace(',', '.')))
    df['Srednia_wartosc_grupy'] = df['Srednia_wartosc_grupy'].apply(lambda x: float(x.replace(',', '.')))
    return df


def load_procedures():
    columns_to_drop = [
        'ID_ProceduryICD9',
        'Mediana',
        'Kod_listy',
        'Kierunkowa',
        'Grupy_JGP_ID_GrupyJGP'

    ]
    path = os.path.join(ROOT_PATH, 'data/signatures/Procedury_ICD9.csv')
    df = load_file(path)
    df = df.drop(columns_to_drop, axis=1)
    df['Udzial'] = df['Udzial'].apply(lambda x: float(x.replace(',', '.')))
    return df[['ID_Zestawienia', 'ID_OWNFZ', 'ID_Kategorii', 'Kod', 'Udzial']]


def load_diagnoses():
    columns_to_drop = [
        'Mediana',
        'Kierunkowa',
        'Kod_listy',
        'Grupy_JGP_ID_GrupyJGP',
        'OWNFZ'
    ]
    path = os.path.join(ROOT_PATH, 'data/signatures/Rozpoznania_ICD10.csv')
    df = load_file(path)
    df['Udzial'] = df['Udzial'].apply(lambda x: float(x.replace(',', '.')))
    df['ID_OWNFZ'] = df['OWNFZ']
    df = df.drop(columns_to_drop, axis=1)
    return df[['ID_Zestawienia', 'ID_OWNFZ', 'ID_Kategorii', 'Kod', 'Udzial']]


def load_products():
    columns_to_drop = [
        'ID_SUMJGP',
        'Grupy_JGP_ID_GrupyJGP',
        'LB_Hosp',
        'Wartosc',
        'Udzial_w_wart_prod_dosumowywanych',
        'Udzial_w_lb_hospitalizacji',
        'Liczba_Hospitalizowanych'
    ]
    path = os.path.join(ROOT_PATH, 'data/signatures/Produkt_sum_JGP.csv')
    df = load_file(path)
    # df['ID_OWNFZ'] = df['OWNFZ']
    df['Liczba_Hospitalizowanych'] = df['LB_Hosp']
    df['Srednia_wartosc'] = df['Wartosc'] / df['LB_Hosp']
    df['Udzial_w_wartosci_produktow'] = df['Udzial_w_wart_prod_dosumowywanych']
    df['Udzial_w_hospitalizacjach'] = df['Udzial_w_lb_hospitalizacji']
    df = df.drop(columns_to_drop, axis=1)
    return df
    # return df[['ID_Zestawienia', 'ID_OWNFZ', 'ID_Kategorii', 'Kod', 'Udzial']]
