import pandas as pd


def get_group_mean_value(df):
    df['Wartosc'] = df['Liczba_pacjentow'] * df['Srednia_wartosc_grupy']
    df_agg = df.groupby(['ID_Zestawienia']).sum()[['Liczba_pacjentow', 'Wartosc']]
    return df_agg['Wartosc'] / df_agg['Liczba_pacjentow']


def get_group_year(df):
    return df.groupby('ID_Zestawienia').first()['Rok']


def get_group_fraction(df):
    return df.groupby('ID_Zestawienia').mean()['Udzial_hospitalizacji']


def get_group_count(df):
    return df.groupby('ID_Zestawienia').sum()['Liczba_pacjentow']


def get_group_code(df):
    return df.groupby('ID_Zestawienia').first()['Kod']


def replace_code(df):
    df['Prefix'] = df['Kod'].apply(lambda x: x[:8])
    df = df[df['Prefix'] == '5.51.01.']
    df['Kod'] = df['Kod'].apply(lambda x: int(x[8:].lstrip('0')))
    return df.drop(['Prefix'], axis=1)


def join_next_year(df):
    df['Rok_nowy'] = df['Rok'] + 1
    return pd.merge(df, df, left_on=['Rok_nowy', 'Kod'], right_on=['Rok', 'Kod'])


def drop_count_less_than(df, n):
    return df[df['Liczba_pacjentow_x'] > n]


def add_value_difference(df):
    df['Roznica_wartosc'] = (df['Srednia_wartosc_y'] / df['Srednia_wartosc_x'] * 100 - 100)
    return df


def add_count_difference(df):
    df['Roznica_liczba_pacjentow'] = df['Liczba_pacjentow_y'] - df['Liczba_pacjentow_x']
    return df

