from tqdm import tqdm
import pandas as pd
from fraud.data import load_groups

from fraud.transformations import get_group_year, get_group_mean_value, get_group_count

def generate_signatures(df, min_frac):
    return df[df['Udzial'] > min_frac].groupby('ID_Zestawienia').apply(lambda x: set(x['Kod']))


def get_most_similar(signatures, n, measure):
    idx_1_list = []
    idx_2_list = []
    measure_list = []

    for idx, signature in tqdm(signatures.iteritems()):
        for idx_inner, signature_inner in signatures.iteritems():
            if idx < idx_inner:
                m = measure(signature, signature_inner)
                if m != 0.0:
                    idx_1_list.append(idx)
                    idx_2_list.append(idx_inner)
                    measure_list.append(m)

    measures = pd.DataFrame({
        'idx_1': idx_1_list,
        'idx_2': idx_2_list,
        'Podobienstwo': measure_list
    })

    return measures.sort_values('Podobienstwo', ascending=False).head(n)



def enrich_most_similar(most_similar, n):

    groups = load_groups()

    enriched = pd.concat([get_group_year(groups), get_group_mean_value(groups), get_group_count(groups)], axis=1).reset_index()
    enriched.columns = ['ID_Zestawienia', 'Rok', 'Srednia_wartosc', 'Liczba_pacjentow']
    idx_1_joined = most_similar.merge(enriched, left_on='idx_1', right_on='ID_Zestawienia')
    idx_2_joined = idx_1_joined.merge(enriched, left_on='idx_2', right_on='ID_Zestawienia', suffixes=['_1', '_2'])
    candidates = idx_2_joined[idx_2_joined['Rok_1'] == idx_2_joined['Rok_2']]
    candidates = candidates.sort_values('Podobienstwo', ascending=False).head(n)
    candidates['Roznica_sredniej_wartosci'] = (candidates['Srednia_wartosc_2'] - candidates['Srednia_wartosc_1']).abs()
    candidates = candidates.sort_values('Roznica_sredniej_wartosci', ascending=False)

    candidates['Drozsza'] = candidates[['Srednia_wartosc_2', 'Srednia_wartosc_1']].max(axis=1)
    candidates['Tansza'] = candidates[['Srednia_wartosc_2', 'Srednia_wartosc_1']].min(axis=1)
    candidates['1_Drozsza'] = candidates['Drozsza'] == candidates['Srednia_wartosc_1']
    candidates['Libczba_pacjentow_drozsza'] = candidates.apply(lambda x: x['Liczba_pacjentow_1'] if x['1_Drozsza'] == True else x['Liczba_pacjentow_2'], axis=1)
    candidates['Libczba_pacjentow_tansza'] = candidates.apply(lambda x: x['Liczba_pacjentow_2'] if x['1_Drozsza'] == True else x['Liczba_pacjentow_1'], axis=1)


    candidates['Sumaryczna_roznica_wartosci'] = (candidates['Drozsza'] - candidates['Tansza']) * candidates['Libczba_pacjentow_drozsza']

    candidates = candidates[candidates['Roznica_sredniej_wartosci'] < 10000].sort_values('Sumaryczna_roznica_wartosci', ascending=False)
    result = candidates[['idx_1', 'idx_2', 'Podobienstwo', 'Roznica_sredniej_wartosci', 'Libczba_pacjentow_drozsza', 'Libczba_pacjentow_tansza', 'Sumaryczna_roznica_wartosci']]

    return result

