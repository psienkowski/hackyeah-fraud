import seaborn as sns
import pandas as pd

import matplotlib.pyplot as plt


def plot_difference_absolute(data, code):
    df = pd.DataFrame(data[data['Kod'] == code])

    sns.scatterplot(df['Rok'], df['Liczba_pacjentow'], label='liczba')
    sns.lineplot(df['Rok'], df['Liczba_pacjentow'])

    sns.scatterplot(df['Rok'], df['Srednia_wartosc'], color='red', label='wartosc')
    sns.lineplot(df['Rok'], df['Srednia_wartosc'], color='red')



def plot_difference_base(data, code):
    df = pd.DataFrame(data[data['Kod'] == code])

    base_count = df['Liczba_pacjentow'].iloc[0]
    base_value = df['Srednia_wartosc'].iloc[0]

    df['Liczba_pacjentow_base'] = [base_count] * len(df)
    df['Srednia_wartosc_base'] = [base_value] * len(df)

    df['Zmiana_srednia_wartosc'] = (df['Srednia_wartosc'] - df['Srednia_wartosc_base']) / df[
        'Srednia_wartosc_base'] * 100
    df['Zmiana_pacjentow'] = (df['Liczba_pacjentow'] - df['Liczba_pacjentow_base']) / df['Liczba_pacjentow_base'] * 100

    sns.scatterplot(df['Rok'], df['Zmiana_pacjentow'], label='liczba')
    sns.lineplot(df['Rok'], df['Zmiana_pacjentow'])

    sns.scatterplot(df['Rok'], df['Zmiana_srednia_wartosc'], color='red', label='wartosc')
    sns.lineplot(df['Rok'], df['Zmiana_srednia_wartosc'], color='red')

    plt.ylim((-50, 80))


def print_candidates(result):
    for i, row in result.iterrows():
        code = row['Kod']
        year = row['Rok_x']
        print('Candidate for upcoding to: {}'.format(int(row['Kod'])))
        print('-----')
        print('Value diff: {}, \tcount diff: {}'.format(round(row['Roznica_wartosc'], 2), round(row['Roznica_liczba_pacjentow'], 2)))
        print('-----')

        print('Candidates for upcodnig from:')
        local_codes = [code + i for i in [-4, -3, -2, -1, 1, 2, 3, 4]]
        for lc in local_codes:
            local = df[(df['Kod'] == lc) & (df['Rok_x'] == year)]
            if len(local) != 0:
                local = local.iloc[0,:]
                print('Value diff: {}, \tcount diff: {}, code: {}'.format(round(local['Roznica_wartosc'], 2), round(local['Roznica_liczba_pacjentow'], 2), local['Kod']))
                print()

        print()