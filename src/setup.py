from setuptools import setup

setup(name='fraud',
      version='0.1',
      author='Pawel Sienkowski',
      author_email='pawel.sienkowski@gmail.com',
      packages=['fraud'])
